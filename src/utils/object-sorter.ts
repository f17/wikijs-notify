export const objectSorter = (obj: Record<string, any>) => {
  const result: Record<string, any> = {}
  let keys = Object.keys(obj)
  keys = keys.sort((left, right) => left.localeCompare(right))
  keys.forEach((key) => {
    const value = obj[key]
    if (!Array.isArray(value) && typeof value === 'object') {
      result[key] = objectSorter(value)
    } else if (Array.isArray(value)) {
      result[key] = value.sort((left, right) => {
        if (left.gameId && right.gameId) {
          return left.gameId - right.gameId
        }
        return left.url.localeCompare(right.url)
      })
    } else {
      result[key] = value
    }
  })
  return result
}
