import * as fs from 'fs'
import { objectSorter } from '../utils/object-sorter'

export const saveJsonToFile = (data: Record<string, any>, filePath: string) => {
  const stringifyData = JSON.stringify(objectSorter(data), null, 2)
  fs.writeFileSync(filePath, stringifyData, {
    flag: 'w+',
  })
}
