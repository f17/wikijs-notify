import { Command, flags } from '@oclif/command'
import * as fs from 'fs'
import fetch, { Headers } from 'node-fetch'
import {
  differenceInDays,
  differenceInHours,
  differenceInMinutes,
  formatISO,
  isBefore,
  parseISO,
  format,
} from 'date-fns'
import { saveJsonToFile } from './utils/save-json-to-file'

class WikijsNotify extends Command {
  static description = 'describe the command here'

  static flags = {
    // add --version flag to show CLI version
    version: flags.version({ char: 'v' }),
    help: flags.help({ char: 'h' }),
    verbose: flags.boolean(),
    wikijs_url: flags.string({
      description: 'Your Wiki.js instance url',
      required: true,
      env: 'WIKIJSNOTIFY_WIKIJS_URL',
    }),
    wikijs_token: flags.string({
      description: 'Wiki.js API token',
      required: true,
      env: 'WIKIJSNOTIFY_WIKIJS_TOKEN',
    }),
    slack_webhook_url: flags.string({
      description: 'Slack webhook url',
      required: true,
      env: 'WIKIJSNOTIFY_WIKIJS_URL',
    }),
    notification_title: flags.string({
      required: false,
      default: 'Wiki update:',
      env: 'WIKIJSNOTIFY_NOTIFICATION_TITLE',
    }),
  }

  static args = []

  async run() {
    const { args, flags } = this.parse(WikijsNotify)

    const now = new Date()
    const dbFilePath = `${this.config.dataDir}/wikijsnotify-notifications.json`
    let notificationMessage = `*${flags.notification_title}*: ${flags.wikijs_url}
`
    const updatedPages: Array<{
      page: any
      pageSummary: string
      updatedAt: Date
    }> = []

    //  create data directory if doesn't exist
    if (!fs.existsSync(this.config.dataDir)) {
      fs.mkdirSync(this.config.dataDir, { recursive: true })
    }
    this.log(this.config.dataDir)

    //  create empty file if doesn't exist
    if (!fs.existsSync(dbFilePath)) {
      saveJsonToFile(
        {
          notificationSent: {},
        },
        dbFilePath
      )
    }

    //  open existing db and load data
    const buffer = fs.readFileSync(dbFilePath, 'utf-8')
    const db = JSON.parse(buffer)
    if (flags.verbose) {
      this.log('   -> Locale db with sent notifications:  ')
      this.log(db)
    }

    //  fetch last updated pages from graphql API
    const myHeaders = new Headers()
    myHeaders.append('Authorization', `Bearer ${flags.wikijs_token}`)
    myHeaders.append('Content-Type', 'application/json')

    const requestOptions: any = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',
    }

    await fetch(
      `${flags.wikijs_url}/graphql?query=query {
      pages{
        list(orderBy:UPDATED, orderByDirection:DESC) {
          title
          updatedAt
          tags
          path
          isPrivate
          isPublished
          description
          privateNS
          id
          }
        }
      }`,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const pages = result.data?.pages?.list ?? []
        pages
          // take only pages:
          //  * to which notification should be send
          //  * published
          .filter((page: any) => {
            const updatedAt = parseISO(page.updatedAt)
            // const diffDays = differenceInDays(now, updatedAt)
            // const diffHours = differenceInHours(now, updatedAt)
            // const diffMinutes = differenceInMinutes(now, updatedAt)

            let notificationShouldBeSent = false
            const lastNotificationForPage = db.notificationSent[page.id] ?? null

            if (lastNotificationForPage) {
              const lastNotificationDate = parseISO(
                lastNotificationForPage.sentAt
              )
              if (isBefore(lastNotificationDate, updatedAt)) {
                notificationShouldBeSent = true
              }
            } else {
              notificationShouldBeSent = true
            }

            if (flags.verbose) {
              this.log(`   -> `)
              this.log(`   -> Analysing page  `)
              this.log(page)
              this.log(
                `   -> lastNotificationForPage: ${lastNotificationForPage}    notificationShouldBeSent:  ${notificationShouldBeSent} `
              )
            }

            return page.isPublished && notificationShouldBeSent
          })
          //  prepare pages
          .forEach((page: any) => {
            const updatedAt = parseISO(page.updatedAt)
            updatedPages.push({
              page: page,
              pageSummary: `${page.id} ${page.title} ${page.path} ${page.locale}`,
              updatedAt,
            })
          })

        return null
      })
      .then(() => {
        if (updatedPages.length > 0) {
          // prepare notification message & update db information to be saved
          updatedPages.forEach((updatedPage) => {
            db.notificationSent[updatedPage.page.id] = {
              sentAt: formatISO(now),
              title: updatedPage.page.title,
            }
            notificationMessage += `
<${flags.wikijs_url}/${updatedPage.page.locale ?? 'pl'}/${
              updatedPage.page.path
            }|${updatedPage.page.title}> :: _${format(
              updatedPage.updatedAt,
              'eeee, dd MMM HH:mm'
            )}_`
          })

          if (flags.verbose) {
            this.log(`   -> `)
            this.log(
              `   -> Saving locale file with updated information about sent notifications `
            )
          }

          // save updated db to the file
          saveJsonToFile(db, dbFilePath)

          if (flags.verbose) {
            this.log(`   -> `)
            this.log(`   -> Sending notification: `)
            this.log(notificationMessage)
          }

          // send notification
          fetch(flags.slack_webhook_url, {
            method: 'POST',
            body: JSON.stringify({
              text: notificationMessage,
            }),
          })
        }
      })
      .catch((error) => this.log('error', error))
  }
}

export = WikijsNotify
