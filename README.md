wikijs-notify
=============



[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@f17/wikijs-notify.svg)](https://npmjs.org/package/@f17/wikijs-notify)
[![Downloads/week](https://img.shields.io/npm/dw/@f17/wikijs-notify.svg)](https://npmjs.org/package/@f17/wikijs-notify)

<!-- toc -->
* [Wikijs-notify](#wikijs-notify)
* [Prerequisites](#prerequisites)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Wikijs-notify

Small script to send notifications via slack webhook about page updates on wiki.js instance.

On the first run, the script will send a notification with information about all pages and save locally a json file with this information: page ID, page title, and the date and time of the last notification.

On subsequent launches, it will compare the date and time of the sent notification with the page update time and it will send notifications about the updated pages after the last notification was sent.

# Prerequisites

* Get your Wiki.js API token from Administration Area (under API Access).
* Configure slack webhook for notifications.

# Usage
<!-- usage -->
```sh-session
$ npm install -g @f17/wikijs-notify
$ wikijs-notify COMMAND
running command...
$ wikijs-notify (-v|--version|version)
@f17/wikijs-notify/0.1.6 darwin-x64 node-v12.18.3
$ wikijs-notify --help [COMMAND]
USAGE
  $ wikijs-notify COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->

<!-- commandsstop -->

## Examples

```
wikijs-notify  --wikijs_url XXX --wikijs_token YYY --slack_webhook_url ZZZ
```

## development

`./bin/run`
